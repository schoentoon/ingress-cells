# Ingress cells

[![GoDoc](https://godoc.org/gitlab.com/schoentoon/ingress-cells?status.svg)](https://godoc.org/gitlab.com/schoentoon/ingress-cells)
[![coverage report](https://gitlab.com/schoentoon/ingress-cells/badges/master/coverage.svg)](https://gitlab.com/schoentoon/ingress-cells/-/commits/master)

This is a simple golang library to turn a location coordinate into an Ingress Cell name. Most of this code is based directly on code from IITC.