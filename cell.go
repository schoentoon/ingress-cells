package cells

import (
	"fmt"
	"math"
	"regexp"
	"strings"
)

type Cell struct {
	i, j  int
	level int
	face  int
}

var face_names = []string{"AF", "AS", "NR", "PA", "AM", "ST"}
var code_words = []string{"ALPHA", "BRAVO", "CHARLIE", "DELTA",
	"ECHO", "FOXTROT", "GOLF", "HOTEL",
	"JULIET", "KILO", "LIMA", "MIKE",
	"NOVEMBER", "PAPA", "ROMEO", "SIERRA",
}

var validateRegex = regexp.MustCompile(fmt.Sprintf(`^(?:(?:(%s)-?)?((?:1[0-6])|(?:0?[1-9]))-?)?(%s)(?:-?((?:1[0-5])|(?:0?\d)))?$`, strings.Join(face_names, "|"), strings.Join(code_words, "|")))

func ValidateCellCode(in string) bool {
	return validateRegex.MatchString(in)
}

func CellFromCoordinates(lat, lng float64, level int) *Cell {
	// LatLngToXYZ
	x, y, z := latlngtoxyz(lat, lng)

	// XYZToFaceUV
	face, u, v := xyztoFaceUV(x, y, z)

	s, t := uvtost(u, v)

	i, j := sttoij(s, t, level)

	out := &Cell{
		i:     i,
		j:     j,
		level: level,
		face:  face,
	}
	return out
}

func latlngtoxyz(lat, lng float64) (float64, float64, float64) {
	d2r := math.Pi / 180.0
	phi := lat * d2r
	theta := lng * d2r
	cosphi := math.Cos(phi)
	return math.Cos(theta) * cosphi,
		math.Sin(theta) * cosphi,
		math.Sin(phi)
}

func xyztoFaceUV(x, y, z float64) (int, float64, float64) {
	xyz := []float64{x, y, z}
	face := largestAbsComponent(xyz)

	if xyz[face] < 0 {
		face += 3
	}

	u, v := faceXYZToUV(face, xyz)

	return face, u, v
}

func faceXYZToUV(face int, xyz []float64) (u, v float64) {
	switch face {
	case 0:
		u = xyz[1] / xyz[0]
		v = xyz[2] / xyz[0]
	case 1:
		u = -xyz[0] / xyz[1]
		v = xyz[2] / xyz[1]
	case 2:
		u = -xyz[0] / xyz[2]
		v = -xyz[1] / xyz[2]
	case 3:
		u = xyz[2] / xyz[0]
		v = xyz[1] / xyz[0]
	case 4:
		u = xyz[2] / xyz[1]
		v = -xyz[0] / xyz[1]
	case 5:
		u = -xyz[1] / xyz[2]
		v = -xyz[0] / xyz[2]
	default:
		panic("We shouldn't be able to get here!")
	}

	return u, v
}

func largestAbsComponent(in []float64) int {
	abs := []float64{math.Abs(in[0]), math.Abs(in[1]), math.Abs(in[2])}
	if abs[0] > abs[1] {
		if abs[0] > abs[2] {
			return 0
		} else {
			return 2
		}
	} else {
		if abs[1] > abs[2] {
			return 1
		} else {
			return 2
		}
	}
}

func uvtost(u, v float64) (float64, float64) {
	single := func(u float64) float64 {
		if u >= 0 {
			return 0.5 * math.Sqrt(1+3*u)
		} else {
			return 1 - 0.5*math.Sqrt(1-3*u)
		}
	}

	return single(u), single(v)
}

func sttoij(i, j float64, level int) (int, int) {
	max := float64(int(1 << level))

	single := func(in float64) int {
		ij := math.Floor(in * max)
		return int(math.Max(0, math.Min(max-1, ij)))
	}

	return single(i), single(j)
}

func (c *Cell) faceAndQuads() []int {
	type e struct {
		pos       int
		character rune
	}

	hilbert := map[rune][]e{
		'a': []e{e{0, 'd'}, e{1, 'a'}, e{3, 'b'}, e{2, 'a'}},
		'b': []e{e{2, 'b'}, e{1, 'b'}, e{3, 'a'}, e{0, 'c'}},
		'c': []e{e{2, 'c'}, e{3, 'd'}, e{1, 'c'}, e{0, 'b'}},
		'd': []e{e{0, 'a'}, e{3, 'c'}, e{1, 'd'}, e{2, 'd'}},
	}

	current := 'a'
	out := []int{}

	checkmask := func(i, mask int) int {
		if i&mask == mask {
			return 1
		}
		return 0
	}

	for i := c.level - 1; i >= 0; i-- {
		mask := 1 << i

		quad_x := checkmask(c.i, mask)
		quad_y := checkmask(c.j, mask)

		entry := hilbert[current][quad_x*2+quad_y]
		out = append(out, entry.pos)
		current = entry.character
	}

	return out
}

func (c *Cell) Name() string {
	cell := c
	if c.face == 1 || c.face == 3 || c.face == 5 {
		// we flip i and j here just like in https://github.com/IITC-CE/ingress-intel-total-conversion/blob/392f0ec0f7a7f4ae91f29ecb109d35fa18a3ba4b/plugins/regions.js#L57-L59
		cell = &Cell{
			face:  c.face,
			i:     c.j,
			j:     c.i,
			level: c.level,
		}
	}

	name := face_names[c.face]

	if cell.level >= 4 {
		regionI := int(cell.i) >> (cell.level - 4)
		regionJ := int(cell.j) >> (cell.level - 4)

		name += fmt.Sprintf("%02d-%s", regionI+1, code_words[regionJ])
	}

	if cell.level >= 6 {
		facequads := cell.faceAndQuads()
		number := facequads[4]*4 + facequads[5]

		name += fmt.Sprintf("-%02d", number)
	}

	return name
}
