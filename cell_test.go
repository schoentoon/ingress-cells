package cells

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidate(t *testing.T) {
	tests := []struct {
		name  string
		valid bool
	}{
		{"NR02-HOTEL-03", true},
		{"NR01-HOTEL-03", true},
		{"NR02-GOLF-04", true},
		{"AM01-CHARLIE-02", true},
		{"ST06-JULIET-11", true},
		{"PA03-ROMEO-09", true},
		{"AF11-GOLF-08", true},
		{"AF14-MIKE-07", true},
		{"AF06-PAPA-00", true},
		{"NR02-HOTEL-01", true},
		{"NR03-GOLF-09", true},
		{"NR03-GOLF-00", true},
		{"NR06-HOTEL-14", true},
		{"AS16-NOVEMBER-08", true},
		{"this is totally not a cell code", false},
	}

	for _, test := range tests {
		assert.Equal(t, test.valid, ValidateCellCode(test.name))
	}
}

func TestCellName(t *testing.T) {
	tests := []struct {
		lat, lng float64
		name     string
	}{
		{52.372831, 4.893679, "NR02-HOTEL-03"},
		{48.858256, 2.294629, "NR01-HOTEL-03"},
		{52.513099, 13.42008, "NR02-GOLF-04"},
		{37.791541, -122.390014, "AM01-CHARLIE-02"},
		{-77.846653, 166.666979, "ST06-JULIET-11"}, // good test case of the flipping part
		{-33.857516, 151.214876, "PA03-ROMEO-09"},
		{-8.825077, 13.237198, "AF11-GOLF-08"},
		{15.553461, 32.502954, "AF14-MIKE-07"},
		{27.951078, -15.522829, "AF06-PAPA-00"},
		{50.846113, 4.355632, "NR02-HOTEL-01"},
		{55.698462, 12.599334, "NR03-GOLF-09"},
		{59.913665, 10.752791, "NR03-GOLF-00"},
		{78.217793, 15.711015, "NR06-HOTEL-14"},
		{39.906444, 116.389697, "AS16-NOVEMBER-08"},
	}

	for _, test := range tests {
		c := CellFromCoordinates(test.lat, test.lng, 6)

		assert.Equal(t, test.name, c.Name())
	}
}

func BenchmarkCellFromCoordinate(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = CellFromCoordinates(52.372831, 4.893679, 6)
	}
}
